/* Copyright © 2015 Oracle and/or its affiliates. All rights reserved. */
package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.SortedCircularDoublyLinkedList;

import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;       

@Path("/cars")
public class CarManager {

	private static CarComparator comp = new CarComparator();
	private static SortedCircularDoublyLinkedList<Car> carList = new SortedCircularDoublyLinkedList<Car>(comp);  


	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCar() {
		Car[] arr = new Car[carList.size()];
		int i = 0;
		for(Car car: carList){
			arr[i] = car;
			i++;
		}
		return arr;
	}            

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		for(Car car: carList){
			if(car.getCarId() == id){
				return car;
			}
		}

		throw new NotFoundException();

	}  


	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car newCar){
		if(carList.isEmpty()) {
			for(int i = 0; i< carList.size(); i++){
				if((carList.get(i).getCarId() == newCar.getCarId()) || (carList.get(i).getCarBrand()==null ||
						carList.get(i).getCarModel()==null) || (carList.get(i).getCarModelOption()==null) ||
						(newCar.getCarPrice()<=0)){
					return Response.status(404).build();
				}

			}
		}
		carList.add(newCar);
		return Response.status(201).build();
	}      

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car newcar){

		for(Car car: carList){
			if(car.getCarId() == newcar.getCarId()){
				carList.remove(car);
				carList.add(newcar);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();      
	}      

	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		for (Car car: carList) {
			if(car.getCarId()== id){
				carList.remove(car);

				return Response.status(Response.Status.OK).build();
			}
		}
		throw new NotFoundException();
	}   
}